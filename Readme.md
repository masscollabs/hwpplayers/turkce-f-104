# Bu projeye neden ihtiyaç duydum
Türkçe klavye düzenlerini incelediğimizde karşımıza çeşitli sorunlar çıkmakta. Örneğin Q klavye ile `altgr shift q` yaparak yazılan (ohm) `Ω` sembolünü F klavyede yazacak herhangi bir kısayol bulunmamaktadır. Bununla birlikte Q klavyede `é` sembolü yazılabilirken büyük hali olan `É` yazmak imkansızdır. Ayrıca bu sembol F klavyede de yer almamaktadır. F klavyede `>` yazmaya yarayan tuş 105 tuşlu klavyelere özel olan tuşta (shift ile j arasındaki tuş) yer alır ve alternatif yazma kısayolu yoktur. Buna karşılık Q klavyede 3 farklı yolla `>` yazmak mümkündür. Bu yüzden 104 tuşlu F klavye kullanmak sorunlara neden olmaktadır...

Bu projede bu gibi problemleri giderip daha tutarlı ve uyumlu klavye düzeni oluşturmayı amaçlamaktayız.

# Klavye Düzeni (F)

![Türkçe F 104 Tuşlu](https://gitlab.com/sulincix/turkce-f-104/-/raw/master/trf.png)

Değişiklikler


1. `altgr j = <` ve `altgr ö = >` şeklinde ekleme yapıldı.

1. `«` tuşu `altgr shift j` ve `»` tuşu `altgr shift ö` kısayollarına taşındı.

1. `¦` tuşu için `altgr shift -` şeklinde ekleme yapıldı.

1. `altgr g` ve `altgr shift g` tuşları ile `ə` ve `Ə` yazılabilir hale getirildi.

1. `altgr shift f` kısayolu `Ω` yazabilir hale getirildi.

# Klavye Düzeni (Q)

![Türkçe Q 104 Tuşlu](https://gitlab.com/sulincix/turkce-f-104/-/raw/master/trq.png)

1. `altgr w` ve `altgr shift w` tuşları ile `ə` ve `Ə` yazılabilir hale getirildi.
